const int stepPin = 3;
const int dirPin = 4;

void setup() {
  // Initialize serial communication at 115200 baud
  Serial.begin(115200, SERIAL_8N1);

  pinMode(LED_BUILTIN, OUTPUT);

  // Initialize stepper pins (steps, direction)
  pinMode(stepPin,OUTPUT);
  pinMode(dirPin,OUTPUT);
}

void setDirection(bool clockwise){
  if(clockwise){
    digitalWrite(dirPin, HIGH);
  }
  else{
    digitalWrite(dirPin, LOW);
  }
}

int go(int steps){
  for(int x = 0; x < steps; x++) {
    digitalWrite(stepPin,HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin,LOW);
    delayMicroseconds(500);
  }
  return(steps);
}

// Receives a String and responds with a String
String handle(String command){

  // for checking that serial communication is working
  if(command == "check"){
    return("ok");
  }

  // Assume that the "command" is a String representation of an integer
  // Convert it to an integer and pass it to setPosition() function
  int steps = command.toInt();
  int status = go(steps);

  return(String(status));
}

void loop() {

  // Check for incoming data
  if (Serial.available() > 0){

    // Read a full line
    String data = Serial.readStringUntil('\n');

    // Remove leading and trailing whitespace
    data.trim();

    // Handle the data and send the response
    String response = handle(data);
    Serial.println(response);
    Serial.flush();

  }
}
