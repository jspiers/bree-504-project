import logging
import serial
from time import sleep

class Arduino(object):
    port = "/dev/ttyACM0"
    baudrate = 115200
    timeout = 20

    def __init__(self):
        logging.debug("serial v{}".format(serial.VERSION))
        self.serial = serial.Serial(port=self.port,
                                    baudrate=self.baudrate,
                                    bytesize=serial.EIGHTBITS,
                                    parity=serial.PARITY_NONE,
                                    stopbits=serial.STOPBITS_ONE,
                                    timeout=self.timeout)
        sleep(3)
        logging.debug("Using port {!r}".format(self.serial.name))

    def terminal(self):
        print 'Enter your commands below.\r\nInsert "exit" to leave the application.'
        while True:
            # get keyboard input
            command = raw_input(">> ")
            # Python 3 users
            # input = input(">> ")
            if command == 'exit':
                self.serial.close()
                break
            else:
                # send the character to the device
                # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
                self.serial.write(command + '\r\n')
                out = ''
                # let's wait one second before reading output (let's give device time to answer)
                sleep(1)
                while self.serial.inWaiting() > 0:
                    out += self.serial.read(1)

                if out != '':
                    print ">>" + out

    def command(self, data, endline="\r\n"):
        # make sure there is only one command
        assert '\n' not in data

        # convert string into binary data (for Python3 compatibility)
        send_data = bytearray(data + endline, "utf-8")

        # send the data
        self.serial.write(send_data)
        #self.serial.flush()
        logging.debug("sent:     {!r}".format(send_data))

        # wait for a response
        recv_data = bytes(self.serial.readline())

        # Alternative approach
        #recv_data = ''
        #while self.serial.inWaiting() > 0:
        #    recv_data += self.serial.read(1)

        logging.debug("received: {!r}".format(recv_data))


        if not recv_data:
            fmt = "readline() timed out after {} seconds"
            logging.warning(fmt.format(self.timeout))
            return None

        # make sure response has CR+LF line ending as expected
        assert recv_data[-2:] == b"\r\n", recv_data

        return recv_data[:-2]
