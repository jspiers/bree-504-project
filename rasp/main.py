#!/usr/bin/env python

import sys
import logging
from time import sleep
from pprint import pprint
from functools import partial

import cv2 as cv
from picamera import PiCamera
from picamera.array import PiRGBArray

from CameraLED import CameraLED
from arduino import Arduino

FULL_ROTATION_STEPS = 200


def extract_roi(img, roi):
    x, y, w, h = roi
    return img[y:y+h, x:x+w]

def middle_third(img):
    height, width = img.shape[:2]
    return img[height//3:-height//3, width//3:-width//3]

def monitor(img, preprocess=None, blocking=False):
    pp_img = preprocess(img) if preprocess else img
    cv.imshow("Monitor", pp_img)
    cv.waitKey(0 if blocking else 1)
    return pp_img

def excess_green(img):
    b, g, r = cv.split(img)
    #means = [channel.mean() for channel in (b, g, r)]
    return g.mean() / img.mean() - 1

class MyCamera(PiCamera):
    
    def __init__(self, *args, **kwargs):
        super(MyCamera, self).__init__(*args, **kwargs)
        self.my_led = CameraLED()
        self.roi = None

    def setLED(self, val):
        if val:
            self.my_led.on()
        else:
            self.my_led.off()
        
    def myConfigure(self):
        self.iso = 1600
        sleep(2)

        self.shutter_speed = self.exposure_speed
        self.exposure_mode = "off"

        # Fixing the auto white balance
        g = self.awb_gains
        self.awb_mode = "off"
        self.awb_gains = g
        
        self.rawCapture = PiRGBArray(self)

    def captureRaw(self):
        self.rawCapture.truncate(0)
        self.capture(self.rawCapture, "bgr")
        img = self.rawCapture.array
        if self.roi is not None:
            return extract_roi(img, self.roi)
        return img.copy()

    def promptForROI(self):
        self.rawCapture.truncate(0)
        self.capture(self.rawCapture, "bgr")
        img = self.rawCapture.array
        x, y, w, h = cv.selectROI(img, False)
        if w < 0:
            x += w
            w = -w
        if h < 0:
            y += h
            h = -h
        self.roi = (x, y, w, h)

    def getROI(self):
        return self.roi

    def setROI(self, roi):
        self.roi = roi
    
    def clearROI(self):
        self.roi = None


class PlatformAngler(object):
    def __init__(self):
        
        # Connect to the Arduino via serial port
        self.arduino = Arduino()
                
        # initialize the camera
        self.camera = MyCamera(framerate=30)

    def check(self):
        response = self.arduino.command("check")
        return response == b"ok", response        
    
    def go(self, steps, increment=None):
        pos = 0
        while pos < steps:
            response = self.arduino.command(str(increment or steps))
            pos += increment
            sleep(1)
    
    def measure(self, num_positions, preview=False, show_captured=True):
        # Verify Arduino connection
        assert self.check()

        # Sets the camera to RGB mode
        self.camera.setLED(True)

        # Start the camera preview
        if preview:
            self.camera.start_preview()

        # Configure camera parameters
        self.camera.myConfigure()

        # Get user to select a region of interest in the image
        self.camera.promptForROI()
        print "ROI", self.camera.getROI()

        # Compute the number of steps per position
        steps = FULL_ROTATION_STEPS // num_positions
        command = str(steps)

        for i in range(num_positions):
            # Advance the stepper motor by 'steps'
            # (except the first iteration, just stay put)
            if i:
                logging.info("command:  {!r}".format(command))
                response = self.arduino.command(command)
                logging.info("response: {!r}".format(response))
            
            averaging = 1 # no averaging
            #preprocess = middle_third
            values = []
            for _ in range(averaging):
                img = self.camera.captureRaw()
                if show_captured:
                    monitor(img)
                values.append(excess_green(img))
            yield i * steps, sum(values) / len(values)        

        cv.destroyAllWindows()
        if preview:
            self.camera.stop_preview()


def main(args):
    fmt = "%(filename)-10s line%(lineno)4s: %(levelname)-7s: %(message)s"
    #logging.basicConfig(format=fmt, level=logging.DEBUG)

    logging.info("Python v{}".format(sys.version.split(' ', 1)[0]))
    logging.info("OpenCV v{}".format(cv.__version__))
    logging.debug("args:     {}".format(args))

    platform_angler = PlatformAngler()
 
    #arduino.terminal()

    result = None
    while True:
        command = raw_input(">> ").strip().split()
        if not command:
            break
        elif command[0].startswith('#'):
            continue
        elif command[0] == "check":
            assert platform_angler.check()
        elif command[0] == "measure":
            if len(command) < 2:
                print "'measure' command requires number of positions as argument (e.g. 'measure 10')"
                continue
            
            num_positions = int(command[1])
            print "Measuring excess green at {} positions".format(num_positions)
            
            val_position = []
            for steps, measured in platform_angler.measure(num_positions):
                print "{:3d} {:5.3f} {}".format(steps, measured, '*' * int(measured*1000))
                val_position.append((measured, steps))
            max_val, max_position = max(val_position)
            print "Maximum value {} is at position {}".format(max_val, max_position)
            result = max_position
        elif command[0] == "go":
            if len(command) > 1:
                steps = int(command[1])
            else:
                steps = int(result)
            platform_angler.go(steps, 2)
        elif command[0] == "setresult":
            if len(command) < 2:
                print "'setresult' command requires number of positions as argument (e.g. 'setresult 10')"
                continue
            result = command[1]

        else:
            print "Invalid command {!r}".format(' '.join(command))


if __name__ == "__main__":
    sys.exit(main(sys.argv))
